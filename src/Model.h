#pragma once

// standard library includes
#include <vector>
#include <string>

// engine includes
#include <tiny_obj_loader.h>

// forward declarations

// file code
class Model
{
public:
	static Model LoadModelFromFile(std::string const& _fileName);

	Model(
		std::vector<tinyobj::shape_t> _shapes,
		std::vector<tinyobj::material_t> _materials
	);

	std::vector<tinyobj::shape_t> const& GetShapes() const;
	std::vector<tinyobj::material_t> const& GetMaterials() const;
private:
	std::vector<tinyobj::shape_t> m_shapes;
	std::vector<tinyobj::material_t> m_materials;
};
