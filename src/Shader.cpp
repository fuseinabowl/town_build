// owning header include
#include "Shader.h"

// standard library includes
#include <sstream>
#include <vector>
#include <iostream>

// engine includes

// file code
using namespace std;
namespace GpuStorage
{

void Shader::CheckShader
(
	GLuint _shaderIdentifier,
	string const& _shaderSource
)
{
	GLint infoLogLength = 0u;
	GL(glGetShaderiv(_shaderIdentifier, GL_INFO_LOG_LENGTH, &infoLogLength));
	vector<GLchar> infoLogData(infoLogLength);
	GLint infoLogActualLength = 0u;
	glGetShaderInfoLog(_shaderIdentifier, infoLogLength, &infoLogActualLength, infoLogData.data());

	cout << infoLogData.data() << endl;

	GLint status = 0u;
	glGetShaderiv(_shaderIdentifier, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		cout << "Compiling failed!" << endl << _shaderSource << endl;
	}
}

GLuint Shader::CreateShader
(
	GLenum _shaderType,
	string const& _loadedShaderSource
)
{
	GLuint shaderIdentifier = glCreateShader(_shaderType);
	GLchar const* loadedShaderCString = _loadedShaderSource.data();
	GLint length = _loadedShaderSource.size() + 1;
	glShaderSource(shaderIdentifier, 1, (const GLchar**)&loadedShaderCString, &length);
	glCompileShader(shaderIdentifier);

	CheckShader(shaderIdentifier, _loadedShaderSource);
	return shaderIdentifier;
}

void Shader::CheckShaderProgram
(
	GLuint _shaderProgramIdentifier
)
{
	GLint infoLogLength = 0u;
	glGetProgramiv(_shaderProgramIdentifier, GL_INFO_LOG_LENGTH, &infoLogLength);
	vector<GLchar> infoLogData(infoLogLength);
	GLint infoLogActualLength = 0u;
	glGetProgramInfoLog(_shaderProgramIdentifier, infoLogLength, &infoLogActualLength, infoLogData.data());

	cout << infoLogData.data() << endl;
}

string Shader::LoadIntoString
(
	istream& _targetStream
)
{
    std::string contents;
    _targetStream.seekg(0, std::ios::end);
    contents.resize(_targetStream.tellg());
    _targetStream.seekg(0, std::ios::beg);
    _targetStream.read(&contents[0], contents.size());
    return(contents);
}

Shader::~Shader
(
)
{
	glDeleteProgram(m_programIdentifier);
}

GLuint Shader::GetProgramIdentifier
(
) const
{
	return m_programIdentifier;
}

}
