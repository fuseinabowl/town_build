#pragma once

// standard library includes

// engine includes


#include "Shader.h"

// forward declarations
namespace GpuStorage
{
	class Model;
};

// file code
class BasicShader : public GpuStorage::Shader
{
public:
	BasicShader();

	void Render(GpuStorage::Model const& _model) const;
private:
	GLint m_modelViewUniformLocation;
	GLint m_projectionUniformLocation;
	GLint m_vertexAttribLocation;
};
