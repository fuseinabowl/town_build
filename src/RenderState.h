#pragma once

// standard library includes

// engine includes
#include <memory>
#include <vector>
#include <SFML/OpenGL.hpp>

// forward declarations

// file code
class Model;
namespace GpuStorage
{

class Model
{
public:
	struct ShapeCpuRecord
	{
		GLsizei m_verticesCount;
		GLsizei m_indicesCount;
	};

	Model(std::vector<ShapeCpuRecord> _cpuShapeRecords, GLuint _vertexBuffer, GLuint _elementBuffer, GLuint _vertexArrayBuffer);
	static std::unique_ptr<Model> LoadModelToGpu(::Model const& _model);
	~Model();

	Model(Model const&) = delete;
	Model& operator=(Model const&) = delete;

	void Render(GLuint _attribIndex) const;
private:
	std::vector<ShapeCpuRecord> m_cpuShapeRecords;
	GLuint m_vertexBufferIdentifier;
	GLuint m_elementBufferIdentifier;
	GLuint m_vertexArrayBufferIdentifier;
};

struct RenderState
{
};

}
