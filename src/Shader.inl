#pragma once

// standard library includes

// engine includes
#include "OpenGlErrors.h"

// forward declarations

// file code
namespace GpuStorage
{

template <typename T_IStream>
Shader::Shader
(
	T_IStream&& _vertexShaderSource,
	T_IStream&& _fragmentShaderSource
)
{
	std::string const vertexSourceLoaded = LoadIntoString(_vertexShaderSource);
	GL(GLuint const vertexShaderIdentifier = CreateShader(GL_VERTEX_SHADER, vertexSourceLoaded));
	std::string const fragmentSourceLoaded = LoadIntoString(_fragmentShaderSource);
	GL(GLuint const fragmentShaderIdentifier = CreateShader(GL_FRAGMENT_SHADER, fragmentSourceLoaded));

	GL(m_programIdentifier = glCreateProgram());
	GL(glAttachShader(m_programIdentifier, vertexShaderIdentifier));
	GL(glAttachShader(m_programIdentifier, fragmentShaderIdentifier));
	GL(glLinkProgram(m_programIdentifier));
	GL(glDeleteShader(vertexShaderIdentifier));
	GL(glDeleteShader(fragmentShaderIdentifier));
	CheckShaderProgram(m_programIdentifier);
}

}
