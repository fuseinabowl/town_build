#pragma once

// standard library includes
#include <iostream>
#include <SFML/OpenGL.hpp>
#include <GL/glu.h>

// engine includes

// forward declarations

// file code
inline void CheckOpenGlErrors
(
)
{
	GLenum const errorCode = glGetError();
	if (GL_NO_ERROR != errorCode)
	{
		std::cout << "error with openGL: " << errorCode << " - " << gluErrorString(errorCode) << std::endl;
		asm ("int $3");
	}
}

#define GL(...) __VA_ARGS__; CheckOpenGlErrors()
