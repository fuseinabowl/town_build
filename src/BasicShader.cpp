// owning header include
#include "BasicShader.h"

// standard library includes
#include <fstream>
#include <array>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

// engine includes
#include "RenderState.h"
#include "OpenGlErrors.h"

// file code
using namespace std;

BasicShader::BasicShader
(
)
	: GpuStorage::Shader(ifstream("data/basic.vshader"), ifstream("data/basic.fshader"))
	, m_modelViewUniformLocation(glGetUniformLocation(GetProgramIdentifier(), "modelview_matrix"))
	, m_projectionUniformLocation(glGetUniformLocation(GetProgramIdentifier(), "projection_matrix"))
	, m_vertexAttribLocation(glGetAttribLocation(GetProgramIdentifier(), "inPosition"))
{
	cout << "mv=" << m_modelViewUniformLocation << endl
		<< "p=" << m_projectionUniformLocation << endl
		<< "vertex=" << m_vertexAttribLocation << endl;
}

void BasicShader::Render
(
	GpuStorage::Model const& _model
) const
{
	GL(glUseProgram(GetProgramIdentifier()));

	auto const model = glm::mat4(1.f);
	auto const view = glm::lookAt(
		glm::vec3(4,3,3),
		glm::vec3(0,0,0),
		glm::vec3(0,1,0)
	);
	auto const projection = glm::perspective(glm::radians(22.f), 800.f / 600.f, 0.1f, 100.f);
	auto const modelView = view * model;
	GL(glUniformMatrix4fv(m_modelViewUniformLocation, 1, GL_FALSE, &modelView[0][0]));
	GL(glUniformMatrix4fv(m_projectionUniformLocation, 1, GL_FALSE, &projection[0][0]));

	//_model.Render(m_vertexAttribLocation);
	GLuint vbo = 0;
	GL(glGenBuffers(1u, &vbo));
	cout << "vbo = " << vbo << endl;
	GL(glBindBuffer(GL_ARRAY_BUFFER, vbo));
	auto const vertices = array<glm::vec3, 3>{{{0,0,0}, {0,1,0}, {1,0,0}}};
	cout << "vertices size = " << sizeof(vertices) << endl;
	GL(glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices.data(), GL_STATIC_DRAW));
	auto const stride = (GLbyte*)&vertices[1] - (GLbyte*)&vertices[0];
	cout << "stride  = " << stride << endl;
	GL(glVertexAttribPointer(m_vertexAttribLocation, 3, GL_FLOAT, GL_FALSE, stride, nullptr));
	GL(glDrawArrays(GL_TRIANGLES, 0u, 3u));
	GL(glBindBuffer(GL_ARRAY_BUFFER, 0u));
	GL(glDeleteBuffers(1u, &vbo));

	GL(glUseProgram(0u));
}

