// owning header include
#include "RenderState.h"

// standard library includes
#include <SFML/OpenGL.hpp>
#include <algorithm>

// engine includes
#include "Model.h"

// file code
using namespace std;
namespace GpuStorage
{

template <typename T_GetSubVectorFunctor>
static GLuint LoadArraysIntoBuffer
(
	vector<tinyobj::shape_t> const& _shapes,
	GLenum _bufferType,
	T_GetSubVectorFunctor&& _getSubVector
)
{
	using BufferedType = decltype(_getSubVector(_shapes.front()));

	size_t numberOfBufferElements = 0u;
	for (tinyobj::shape_t const& shape : _shapes)
	{
		numberOfBufferElements += _getSubVector(shape).size();
	}
	GLsizeiptr const modelBufferSize = sizeof(BufferedType) * numberOfBufferElements;

	GLuint bufferIdentifier = 0u;
	glGenBuffers(1u, &bufferIdentifier);
	glBindBuffer(_bufferType, bufferIdentifier);
	glBufferData(_bufferType, modelBufferSize, nullptr, GL_STATIC_DRAW);

	GLsizeiptr nextSubBufferWriteLocation = 0u;
	for (auto const& shape : _shapes)
	{
		auto const& subVector = _getSubVector(shape);
		glBufferSubData(_bufferType, nextSubBufferWriteLocation, sizeof(BufferedType) * subVector.size(), subVector.data());
		nextSubBufferWriteLocation += subVector.size();
	}
	glBindBuffer(_bufferType, 0u);

	return bufferIdentifier;
}

unique_ptr<Model> Model::LoadModelToGpu
(
	::Model const& _model
)
{
	static_assert(std::is_same<float, GLfloat>::value, "tinyobjloader loads vertices into floats, not GLfloats. We'll need custom converter code.");
	GLuint vertexBufferIdentifier = LoadArraysIntoBuffer(_model.GetShapes(), GL_ARRAY_BUFFER, [](auto const& shape){return shape.mesh.positions;});
	static_assert(std::is_same<unsigned int, GLuint>::value, "tinyobjloader loads indices into unsigned ints, not GLuints. We'll need custom converter code.");
	GLuint elementBufferIdentifier = LoadArraysIntoBuffer(_model.GetShapes(), GL_ELEMENT_ARRAY_BUFFER, [](auto const& shape){return shape.mesh.indices;});

	GLuint vertexArrayBufferIdentifier = 0u;
	glGenVertexArrays(1u, &vertexArrayBufferIdentifier);
	glBindVertexArray(vertexArrayBufferIdentifier);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferIdentifier);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferIdentifier);
	glBindVertexArray(0u);

	vector<ShapeCpuRecord> records(_model.GetShapes().size());
	transform(_model.GetShapes().begin(), _model.GetShapes().end(), records.begin(), []
		(tinyobj::shape_t const& shape)
		{return ShapeCpuRecord{(GLsizei)shape.mesh.positions.size(), (GLsizei)shape.mesh.indices.size()};}
	);

	return make_unique<Model>(records, vertexBufferIdentifier, elementBufferIdentifier, vertexArrayBufferIdentifier);
}

Model::Model
(
	std::vector<ShapeCpuRecord> _cpuShapeRecords,
	GLuint _vertexBuffer,
	GLuint _elementBuffer,
	GLuint _vertexArrayBuffer
)
	: m_cpuShapeRecords(_cpuShapeRecords)
	, m_vertexBufferIdentifier(_vertexBuffer)
	, m_elementBufferIdentifier(_elementBuffer)
	, m_vertexArrayBufferIdentifier(_vertexArrayBuffer)
{
}

Model::~Model
(
)
{
	glDeleteVertexArrays(1u, &m_vertexArrayBufferIdentifier);
	GLuint buffers[2u] {m_vertexBufferIdentifier, m_elementBufferIdentifier};
	glDeleteBuffers(2u, buffers);
}

void Model::Render
(
	GLuint _attribIndex
) const
{
	glBindVertexArray(m_vertexArrayBufferIdentifier);
	glEnableVertexAttribArray(0u);

	GLsizei currentVertexOffset = 0u;
	GLsizei currentElementOffset = 0u;
	for (auto const& cpuShape : m_cpuShapeRecords)
	{
		glVertexAttribPointer(_attribIndex, 3, GL_FLOAT, GL_FALSE, 0, ((GLfloat*)nullptr) + currentVertexOffset);
		glDrawElements(GL_TRIANGLES, cpuShape.m_indicesCount, GL_UNSIGNED_INT, ((GLuint*)nullptr) + currentElementOffset);

		currentVertexOffset += cpuShape.m_verticesCount;
		currentElementOffset += cpuShape.m_indicesCount;
	}

	glDisableVertexAttribArray(0u);
	glBindVertexArray(0u);
}

}
