// owning header include
#include "Model.h"

// standard library includes
#include <stdint.h>
#include <fstream>
#include <cstring>
#include <iostream>

// engine includes
#include <tiny_obj_loader.h>

#define debugger_crash asm("int $3")

// file code
using namespace std;

Model Model::LoadModelFromFile
(
	string const& _fileName
)
{
	vector<tinyobj::shape_t> shapes;
	vector<tinyobj::material_t> materials;
	string errorString;
	bool const loadSuccessful = tinyobj::LoadObj(shapes, materials, errorString, _fileName.c_str());
	if (!errorString.empty())
	{
		cerr << errorString << endl;
	}
	if (!loadSuccessful)
	{
		debugger_crash;
	}

	return Model(shapes, materials);
}

Model::Model
(
	std::vector<tinyobj::shape_t> _shapes,
	std::vector<tinyobj::material_t> _materials
)
	: m_shapes(_shapes)
	, m_materials(_materials)
{
}

std::vector<tinyobj::shape_t> const& Model::GetShapes
(
) const
{
	return m_shapes;
}

std::vector<tinyobj::material_t> const& Model::GetMaterials
(
) const
{
	return m_materials;
}
