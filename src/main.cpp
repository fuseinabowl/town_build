// owning header include
// (none)

// standard library includes
#include <iostream>
#include <fstream>
#include <memory>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>

// engine includes
#include "Model.h"
#include "RenderState.h"
#include "BasicShader.h"

// file code

using namespace std;
struct RenderState
{
	GLuint m_vbo;
};

int main()
{
	cout << "loading model" << endl;
	auto const loadedModel = Model::LoadModelFromFile("external/tinyobjloader/cube.obj");
	cout << "model loaded" << endl;

	sf::VideoMode videoMode(800,600);
	sf::ContextSettings contextSettings;
	contextSettings.majorVersion = 3;
	contextSettings.minorVersion = 1;

	glViewport(0,0, 800,600);

	sf::RenderWindow mainWindow(videoMode, "Town Builder", sf::Style::Default, contextSettings);
	sf::Event event;

	auto const modelOnGpu = GpuStorage::Model::LoadModelToGpu(loadedModel);
	auto const shader = make_unique<BasicShader>();

	while (mainWindow.isOpen())
	{
		while (mainWindow.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				mainWindow.close();
			}
		}

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		shader->Render(*modelOnGpu);

		mainWindow.display();
	}

	return 0;
}
