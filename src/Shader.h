#pragma once

// standard library includes
#include <istream>
#include <SFML/OpenGL.hpp>

// engine includes

// forward declarations

// file code
namespace GpuStorage
{
class Model;

class Shader
{
public:
	template <typename T_IStream>
	Shader(T_IStream&& _vertexShaderSource, T_IStream&& _fragmentShaderSource);
	~Shader();

	Shader(Shader const&) = delete;
	Shader& operator=(Shader const&) = delete;

	GLuint GetProgramIdentifier() const;
private:
	static void CheckShader(GLuint _shaderIdentifier, std::string const& _shaderSource);
	static GLuint CreateShader(GLenum _shaderType, std::string const& _loadedShaderSource);
	static void CheckShaderProgram(GLuint _programIdentifier);
	static std::string LoadIntoString(std::istream& _targetStream);

	GLuint m_programIdentifier;
};

}

#include "Shader.inl"
