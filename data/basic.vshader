#version 140

uniform mat4 projection_matrix;
uniform mat4 modelview_matrix;

in vec3 inPosition;

void main() {
	gl_Position = projection_matrix * modelview_matrix * vec4(inPosition, 1.0);
}

